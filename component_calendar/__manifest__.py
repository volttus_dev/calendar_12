# -*- coding: utf-8 -*-
{
    'name': "VA Component Calendar",
    'version': "1.0",
    'summary': "VA Calendar",

    'author': "VoltusAutomation, KRKA",
    'website': "http://www.voltusautomation.com/component_calendar",
    'category': "Enterprise Component Management",

    # any module necessary for this one to work correctly
    'depends': ['base', 'calendar'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/component_calendar_view.xml',
        'data/mail_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
